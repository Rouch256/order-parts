from fastapi import FastAPI, BackgroundTasks
from .model.item import Item
from .database_model.order import Order
from .services.publisher import publish_message_to_queue
from playhouse.shortcuts import model_to_dict   
import json


app = FastAPI()


@app.post("/api/v1/orders")
async def post_orders(item: Item, background_tasks: BackgroundTasks):
    """
    Endpoint takes three parameters as JSON body request:

    Args:
      Item: data model type. To more details check item.py docs.

    Endpoint pushes the data to the RabbitMQ message broker.
    """

    background_tasks.add_task(
        publish_message_to_queue, json.dumps(item.dict()))

    return item


@app.get("/api/v1/orders")
async def get_orders():
    """
    Endpoint returns all orders from database.
    """

    query = Order.select()
    orders = list(query)
    items = []

    for order in orders:
        items.append(model_to_dict(order))

    return items
