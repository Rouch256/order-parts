from .base_model import BaseModel
from peewee import AutoField, CharField, IntegerField


class Order(BaseModel):
    """
    Order model class. Contains information about order
    created by user.

    Params:
    - order_id: auto generated unique id field
    - part_type: type of part to order
    - part_name: name of part to order
    - quantity: amount of parts to order
    """

    order_id = AutoField(primary_key=True)
    part_type = CharField(max_length=100)
    part_name = CharField(max_length=255)
    quantity = IntegerField()
