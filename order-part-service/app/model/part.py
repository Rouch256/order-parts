from enum import Enum


class PartType(Enum):
    """Enum stores to which elements parts can be ordered.
    """

    FALCON = "Falcon"
    LIGHTSABRE = "Lightsabre"

    @classmethod
    def has_value(cls, value):
        """Checks if part type exists in current class.

        Returns:
            True if part exist.
        """

        return any(value == item.value for item in cls)
