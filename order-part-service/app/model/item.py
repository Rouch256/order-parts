from .part import PartType
from pydantic import BaseModel
from pydantic import validator


class Item(BaseModel):
    """Model class for item.

    Args:
        part_type: (str) - type of part to order
        part_name: (str) - the name of part to order
        quantity: (int) - amount of items to order,
            quantity must be greater or equal zero.
    """
    part_type: str
    part_name: str
    quantity: int

    @validator('part_type')
    def check_if_type_of_part_exist(cls, value):
        """
        Checks if part type is defined in software. If not raises
        a ValueError.

        Returns:
            Value if validation pass, Exceprion otherwise.

        Raises:
            ValueError: if value will not pass the validation.
        """

        if PartType.has_value(value):
            return value
        else:
            raise ValueError(
                'This type of part {} is not avaliable to order!'
                .format(value))

    @validator('part_name')
    def check_if_name_of_part_is_not_empty(cls, value):
        """
        Checks if user send the part name in the request. If not raises
        a ValueError.

        Returns:
            Value if validation pass, Exceprion otherwise.

        Raises:
            ValueError: if value will not pass the validation.
        """

        if value and value.strip(" "):
            return value
        else:
            raise ValueError('Name of part must be specified!')

    @validator('quantity')
    def check_if_quantity_is_not_zero(cls, value):
        """
        Checks if user add positive quantity. If not raises
        a ValueError.

        Returns:
            Value if validation pass, Exceprion otherwise.

        Raises:
            ValueError: if value will not pass the validation.
        """

        if value > 0:
            return value
        else:
            raise ValueError('Quantity must be greater than zero!')
