import pika


RABBITMQ_HOST = "RabbitMQ"
EXCHANGE = "orders-exchange"
EXCHANGE_TYPE = "direct"
ROUTING_KEY = "orders-routing-key"
QUEUE = "orders-queue"


def publish_message_to_queue(message):
    """
    Publish data to RabbitMQ queue.

    Params:
        - message: Data to push to queue.
    """

    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=RABBITMQ_HOST))
    channel = connection.channel()

    channel.exchange_declare(
        exchange=EXCHANGE,
        exchange_type=EXCHANGE_TYPE,
        durable=True)

    channel.queue_declare(queue=QUEUE, durable=True)

    channel.basic_publish(
        exchange=EXCHANGE,
        routing_key=ROUTING_KEY,
        body=message,
        properties=pika.BasicProperties(
            delivery_mode=2
        ))

    print("%r sent to exchange %r with data: %r" % (
        ROUTING_KEY,
        EXCHANGE,
        message))

    connection.close()
