## Installation
First the docker and docker-compose must be installed.
Installation steps for Arch Linux:

1. sudo pacman -Syu
2. sudo pacman -S docker docker-compose
3. sudo systemctl start docker
4. sudo systemctl enable docker
5. sudo usermod -aG docker $USER // Configuration for running docker without root privileges or without having to add sudo everytime
6. reboot

## How to run?

To run open teminal and type the command:
```docker-compose up```

---

## Endpoints

**Get all orders**
----
  Returns json data with all orders stored in database.
  
* **URL**
    /api/v1/orders

* **Method**
    `GET`
	
* **Data Params**
    None
    
**Send order to message broker**
----
  Returns success when valid data were passed or error with information what was wrong.

  * **URL**
    /api/v1/orders

* **Method**
    `POST`
	
* **Data Params**
    - part_type: Falcon/Lightsabre
    - part_name: The name of part to order
    - quantity: (int) - amount of items to order, quantity must be greater or equal zero.

---