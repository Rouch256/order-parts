from peewee import PostgresqlDatabase, Model


postgres_database = PostgresqlDatabase(
    'orderdb',
    user='user',
    password='user',
    host='postgres-database',
    port=5432)


class BaseModel(Model):
    """A base model that will use our Postgresql database."""

    class Meta:
        database = postgres_database
