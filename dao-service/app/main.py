import pika
from model.order import Order
from utils.deserializer import deserialize_data
from peewee import ProgrammingError, OperationalError


RABBITMQ_HOST = "rabbitmq"
EXCHANGE = "orders-exchange"
EXCHANGE_TYPE = "direct"
ROUTING_KEY = "orders-routing-key"
QUEUE = "orders-queue"


def save_data_to_database(ch, method, properties, body):
    """
    Saves data from queue to database. Data come in json format.

    Args:
        body - data from request.
    """
    item = deserialize_data(body)

    order_to_save = Order(
        part_type=item['part_type'],
        part_name=item['part_name'],
        quantity=item['quantity']
    )

    order_to_save.save()


if __name__ == "__main__":
    try:
        Order.create_table()
    except(OperationalError, ProgrammingError):
        print("Table already exist!")

    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=RABBITMQ_HOST))
    channel = connection.channel()

    channel.queue_declare(queue=QUEUE, durable=True)

    channel.basic_consume(
        queue=QUEUE,
        on_message_callback=save_data_to_database,
        auto_ack=True)

    channel.start_consuming()
