import json


def deserialize_data(data):
    return json.loads(data)
