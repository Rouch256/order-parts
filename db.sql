-- Database: ordersdb

-- DROP DATABASE ordersdb;

CREATE DATABASE ordersdb
    WITH 
    OWNER = "user"
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

CREATE TABLE orders(
	order_id SERIAL PRIMARY KEY,
	order_type VARCHAR(100) NOT NULL,
	order_name VARCHAR(255) NOT NULL,
	quantity INTEGER NOT NULL
);
